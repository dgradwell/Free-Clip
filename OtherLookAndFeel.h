#ifndef OTHERLOOKANDFEEL_H_INCLUDED
#define OTHERLOOKANDFEEL_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#define KNOBC 15, 58, 80

//==============================================================================

class OtherLookAndFeel : public LookAndFeel_V3
{
public:
	OtherLookAndFeel()
	{
		setColour(Slider::rotarySliderFillColourId, Colours::red);
	}

	void drawRotarySlider(Graphics& g, int x, int y, int width, int height, float sliderPos,
		const float rotaryStartAngle, const float rotaryEndAngle, Slider& slider) override
	{
		//...
		const float radius = jmin(width / 2, height / 2) - 4.0f;
		const float centreX = x + width * 0.5f;
		const float centreY = y + height * 0.5f;
		const float rx = centreX - radius;
		const float ry = centreY - radius;
		const float rw = radius * 2.0f;
		const float angle = rotaryStartAngle + sliderPos * (rotaryEndAngle - rotaryStartAngle);

		// fill
		g.setColour(Colour(KNOBC));
		g.fillEllipse(rx, ry, rw, rw);
		// outline
		g.setColour(Colours::grey);
		g.drawEllipse(rx, ry, rw, rw, 3.0f);

		Path p;
		const float pointerLength = radius * 0.66f;
		const float pointerThickness = 7.0f;
		p.addRectangle(-pointerThickness * 0.5f, -radius, pointerThickness, pointerLength);
		p.applyTransform(AffineTransform::rotation(angle).translated(centreX, centreY));

		g.setColour(Colours::white);
		g.fillPath(p);
	}

	void drawToggleButton(Graphics& g, ToggleButton& button,
		bool isMouseOverButton, bool isButtonDown)
	{
		float fontSize = jmin(15.0f, button.getHeight() * 0.75f);
		const float tickWidth = fontSize * 1.1f;

		drawTickBox(g, button, 4.0f, (button.getHeight() - tickWidth) * 0.5f,
			tickWidth, tickWidth,
			button.getToggleState(),
			button.isEnabled(),
			isMouseOverButton,
			isButtonDown);

		g.setColour(button.findColour(ToggleButton::textColourId));
		g.setFont(fontSize);

		if (!button.isEnabled())
			g.setOpacity(0.5f);

		const int textX = (int)tickWidth + 5;

		g.drawFittedText(button.getButtonText(),
			textX, 0,
			button.getWidth() - textX - 2, button.getHeight(),
			Justification::centredLeft, 10);
	}

};

class OtherLookAndFeel2 : public LookAndFeel_V3
{
public:
	OtherLookAndFeel2()
	{
		setColour(Slider::rotarySliderFillColourId, Colours::red);
	}

	void drawRotarySlider(Graphics& g, int x, int y, int width, int height, float sliderPos,
		const float rotaryStartAngle, const float rotaryEndAngle, Slider& slider) override
	{
		//...
		const float radius = jmin(width / 2, height / 2) - 4.0f;
		const float centreX = x + width * 0.5f;
		const float centreY = y + height * 0.5f;
		const float rx = centreX - radius;
		const float ry = centreY - radius;
		const float rw = radius * 2.0f;
		const float angle = rotaryStartAngle + sliderPos * (rotaryEndAngle - rotaryStartAngle);

		// fill
		//Colour()
		g.setColour(Colour(KNOBC));
		g.fillEllipse(rx, ry, rw, rw);
		// outline
		g.setColour(Colours::silver);
		g.drawEllipse(rx, ry, rw, rw, 3.0f);
		g.setColour(Colours::gold);

		const float innerRadius = radius * 0.2f;
		const float thickness = 0.7f;
		Path p;
		p.addTriangle(-innerRadius, 0.0f,
			0.0f, -radius * thickness * 1.1f,
			innerRadius, 0.0f);

		p.addEllipse(-innerRadius, -innerRadius, innerRadius * 2.0f, innerRadius * 2.0f);

		g.fillPath(p, AffineTransform::rotation(angle).translated(centreX, centreY));

	}
};

#endif  // OTHERLOOKANDFEEL_H_INCLUDED
